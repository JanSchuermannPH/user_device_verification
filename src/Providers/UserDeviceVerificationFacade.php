<?php

namespace janschuermannph\udv;

use Illuminate\Support\Facades\Facade;

/**
 * UuidFacade
 *
 */
class UserDeviceVerificationFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'udv';
    }
}
