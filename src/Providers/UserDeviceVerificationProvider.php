<?php

namespace janschuermannph\udv;

use Illuminate\Support\ServiceProvider;

class UserDeviceVerificationProvider extends ServiceProvider
{
    protected $defer = false;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__ . '/../config/udv_config.php';
        $this->publishes([$configPath => $this->getConfigPath()], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__.'/../config/udv_config.php';
        $this->mergeConfigFrom($configPath,'udv');
    }
    /**
     * Get the config path
     *
     * @return string
     */
    protected function getConfigPath()
    {
        return config_path('udv.php');
    }
}
