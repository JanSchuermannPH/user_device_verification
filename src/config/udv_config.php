<?php
use App\User;

$enabled = true;
$except = [];
$store_known_devices_in_table = false;
$known_devices_table = null;
$known_devices = [
    'model' => User::class,
    'field' => 'known_devices'
];